package sample;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.*;

//0
//***M1M2M3

public class Controller {

    private final static String ALGORITHM = "AES";
    private final static String ERROR = "err";
    private final static String DEFAULT = "none";
    private String master_password = DEFAULT;
    private String file_path_btnnames, file_path_codes;
    private byte[] keyValue;

    @FXML
    public TextField txt;
    @FXML
    public ScrollPane scr;
    @FXML
    public PasswordField pswtxt;
    @FXML
    public Button btnClose;
    VBox root = new VBox();
    @FXML
    private AnchorPane ap;

    @FXML
    public void initialize() throws IOException {

        List<Button> lst_btn = new ArrayList<Button>();
        scr.setStyle("-fx-unit-increment: 1; -fx-block-increment: 1; ");
        String path = new File(".").getCanonicalPath();
        String file_s = (path + "/Settings/").replace("\\", "/");
        file_path_btnnames = file_s + "buttonsnames.properties";
        file_path_codes = file_s + "codes.properties";
        final File dir_s = new File(file_s);
        makeDir(dir_s);
        createFile(file_path_btnnames);
        createFile(file_path_codes);

        List<String> btn_names = new ArrayList<>();
        readProperties(file_path_btnnames, "btn_names", null, btn_names);

        for (int i = 0; i < btn_names.size(); i++) {

            Button btn = new Button(btn_names.get(i));
            btnOff(btn);
            alignPosition(btn_names, btn);

            btn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                    if (!master_password.equals(DEFAULT)) {

                        String[] spl = event.getSource().toString().split("'");
                        String btn_name = spl[1];

                        for (int j = 0; j < btn_names.size(); j++) {
                            if (btn_name.equals(btn_names.get(j))) {
                                btnOn(lst_btn.get(j));
                            }
                        }
                        pswLogic(btn_name);

                        Timer timer = new Timer();
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                txt.clear();
                            }
                        }, 1000);
                    }
                }
            });
            lst_btn.add(btn);

        }

        root.getChildren().addAll(lst_btn);
        root.setSpacing(10);
        scr.setContent(root);
        scr.setPannable(true);
        txt.clear();

        ap.setOnMouseClicked((new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                String btnclick = String.valueOf(event.getButton());
                if (btnclick.equals("SECONDARY")) {
                    onbtnClose();
                }
            }
        }));

        scr.setStyle("-fx-background-color: transparent; -fx-border-color: #cdcdcd;");
        pswtxt.setText("****");
        Platform.runLater(() -> pswtxt.requestFocus());
    }


    private void pswLogic(String btn_name) {

        if (!txt.getText().equals("")) {

            try {
                writeProperties(file_path_codes, btn_name, encrypt(txt.getText()));
                txt.setText("Encrypted and saved: " + btn_name);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            final ClipboardContent clipboardContent = new ClipboardContent();
            StringBuilder prop = new StringBuilder();
            readProperties(file_path_codes, btn_name, prop, null);
            String Decrypt = decrypt(prop.toString());
            if (!Decrypt.equals(ERROR)) {
                clipboardContent.putString(Decrypt);
                Clipboard.getSystemClipboard().setContent(clipboardContent);
                txt.setText("Сopied to clipboard: " + btn_name);
            } else
                txt.setText("Error decrypt: " + btn_name);
        }
    }


    private void makeDir(File dir_s) {
        if (!dir_s.exists()) {
            dir_s.mkdirs();
        }
    }

    private void alignPosition(List<String> btn_names, Button btn) {
        if (btn_names.size() > 3) {
            btn.setPrefWidth(200);
        } else btn.setPrefWidth(220);
    }


    private void createFile(String file_name) {

        File newFile = new File(file_name);
        try {
            newFile.createNewFile();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }


    private void readProperties(String file_name, String propRead, StringBuilder prop, List<String> properties) {

        if (Files.exists(Paths.get(file_name))) {
            byte[] readIn = new byte[0];
            try {
                readIn = Files.readAllBytes(Paths.get(file_name));
            } catch (IOException e) {
                e.printStackTrace();
            }
            String replacer = new String(readIn, StandardCharsets.UTF_8);

            try (StringReader inp = new StringReader(replacer)) {

                Properties appProps = new Properties();
                appProps.load(inp);
                String readData = appProps.getProperty(propRead);

                if (properties != null) {
                    String[] list = readData.split(",");
                    for (int i = 0; i < list.length; i++) {
                        properties.add(list[i].trim());
                    }
                } else if (prop != null) {
                    prop.append(appProps.getProperty(propRead));
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void writeProperties(String file_name, String propName, String propData) throws IOException {

        if (Files.exists(Paths.get(file_name))) {

            byte[] readIn = Files.readAllBytes(Paths.get(file_name));
            String replacer = new String(readIn, StandardCharsets.UTF_8);

            try (StringReader inp = new StringReader(replacer)) {

                Properties appProps = new Properties();
                appProps.load(inp);
                appProps.setProperty(propName, propData);
                inp.close();

                try (FileOutputStream out = new FileOutputStream(file_name)) {
                    appProps.store(out, "Properties");
                    out.close();
                }

            }

        }
    }


    private Cipher getCipher() {

        try {
            final Cipher instance = Cipher.getInstance(ALGORITHM);
            return instance;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
        throw new UnsupportedOperationException("Cipher instance Unsupported Error!");
    }

    private String encrypt(String valueToEnc) {
        Key key = generateKey();
        try {
            Cipher c = getCipher();
            c.init(Cipher.ENCRYPT_MODE, key);
            byte[] encValue = c.doFinal(valueToEnc.getBytes());
            String encryptedValue = new BASE64Encoder().encode(encValue);
            return encryptedValue;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ERROR;
    }

    private String decrypt(String encryptedValue) {

        try {
            Key key = generateKey();
            Cipher c = getCipher();
            c.init(Cipher.DECRYPT_MODE, key);
            byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedValue);
            byte[] decValue = c.doFinal(decordedValue);
            String decryptedValue = new String(decValue);
            return decryptedValue;
        } catch (Exception e) {
            // e.printStackTrace();
        }
        return ERROR;
    }

    private Key generateKey() {
        keyValue = (master_password + "Gf12AczQtRwi").getBytes();
        Key key = new SecretKeySpec(keyValue, ALGORITHM);
        return key;
    }


    private boolean btnOn(Button btn) {

        btn.setStyle("-fx-background-radius: 0;  -fx-background-color: #ffffff,\n" +
                "linear-gradient( #daeaf1 20%,  #abebc5 20%, #ffffff 10%, #ffffff 10%,  #abebc5 20%,  #daeaf1 20%);");

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                btnOff(btn);
            }
        }, 1000);

        return true;
    }

    private boolean btnOff(Button btn) {

        btn.setStyle("-fx-background-radius: 0; -fx-background-color:#ffffff,\n" +
                "linear-gradient(#a3cadb 20%,  #daeaf1 20%, #ffffff 10%, #ffffff 10%, #daeaf1 20%,  #a3cadb 20%);"
        );

        return false;
    }


    @FXML
    private void onbtnClose() {
        Stage stage = (Stage) ap.getScene().getWindow();
        stage.hide();
    }


    public void onpswtxt() {

        if (!pswtxt.getText().equals("****") && pswtxt.getText().getBytes().length == 4) {
            master_password = pswtxt.getText();
            pswtxt.setStyle("-fx-background-radius: 0; -fx-background-color:transparent \n");
            pswtxt.clear();
            txt.clear();
            pswtxt.setEditable(false);
            pswtxt.setVisible(false);
            Platform.runLater(() -> root.requestFocus());
        }

    }

    public void onscroll(ScrollEvent scrollEvent) {

        double deltaY = scrollEvent.getDeltaY() * 0.0001;
        scr.setVvalue(scr.getVvalue() - deltaY);
        double width = scr.getContent().getBoundsInLocal().getWidth();
        double vvalue = scr.getVvalue();
        scr.setVvalue(vvalue + -deltaY / width);
    }


}

